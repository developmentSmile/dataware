function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    //21.001926, -101.508269
    center: {lat: 21.001926, lng: -101.508269},
    zoom: 15
});

  var iconBase = 'http://marketing-queretaro.com/markers/';

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(21.001926, -101.508269),
    map: map,
    icon: iconBase + '32x32azul.png',
    title: '¡Encuentranos aquí!'
  });

}

